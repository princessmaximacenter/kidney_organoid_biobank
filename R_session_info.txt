R version 3.6.0 (2019-04-26)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 16.04.6 LTS

Matrix products: default
BLAS:   /usr/lib/libblas/libblas.so.3.6.0
LAPACK: /usr/lib/lapack/liblapack.so.3.6.0

locale:
[1] en_US.UTF-8

attached base packages:
[1] parallel  stats4    stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] dplyr_0.8.3            SingleR_1.0.1          reshape2_1.4.3         ggplot2_3.2.1          IDPmisc_1.1.19         clusterProfiler_3.12.0 org.Hs.eg.db_3.8.2     AnnotationDbi_1.46.1   IRanges_2.18.3        
[10] S4Vectors_0.22.1       Biobase_2.44.0         BiocGenerics_0.30.0    zeallot_0.1.0          SCutils_1.60           Matrix_1.2-17          Seurat_3.0.2           openxlsx_4.1.0.1      

loaded via a namespace (and not attached):
  [1] reticulate_1.13             R.utils_2.9.0               tidyselect_0.2.5            RSQLite_2.1.4               htmlwidgets_1.5.1           grid_3.6.0                  BiocParallel_1.18.1        
  [8] Rtsne_0.15                  munsell_0.5.0               codetools_0.2-16            ica_1.0-2                   future_1.15.1               withr_2.1.2                 colorspace_1.4-1           
 [15] GOSemSim_2.10.0             rstudioapi_0.10             ROCR_1.0-7                  pbmcapply_1.5.0             DOSE_3.10.2                 gbRd_0.4-11                 listenv_0.8.0              
 [22] Rdpack_0.11-0               urltools_1.7.3              GenomeInfoDbData_1.2.1      polyclip_1.10-0             bit64_0.9-7                 farver_2.0.1                pheatmap_1.0.12            
 [29] vctrs_0.2.0                 doParallel_1.0.15           R6_2.4.1                    GenomeInfoDb_1.20.0         rsvd_1.0.2                  locfit_1.5-9.1              DelayedArray_0.10.0        
 [36] bitops_1.0-6                fgsea_1.10.0                gridGraphics_0.4-1          assertthat_0.2.1            promises_1.1.0              SDMTools_1.1-221.1          scales_1.1.0               
 [43] ggraph_1.0.2                enrichplot_1.4.0            gtable_0.3.0                npsurv_0.4-0                globals_0.12.5              rlang_0.4.2                 splines_3.6.0              
 [50] lazyeval_0.2.2              europepmc_0.3               backports_1.1.5             httpuv_1.5.2                qvalue_2.16.0               tools_3.6.0                 ggplotify_0.0.4            
 [57] gplots_3.0.1.1              RColorBrewer_1.1-2          ggridges_0.5.1              Rcpp_1.0.3                  plyr_1.8.4                  zlibbioc_1.30.0             progress_1.2.2             
 [64] purrr_0.3.3                 RCurl_1.95-4.12             prettyunits_1.0.2           pbapply_1.4-1               viridis_0.5.1               cowplot_1.0.0               zoo_1.8-6                  
 [71] SummarizedExperiment_1.14.1 ggrepel_0.8.1               cluster_2.1.0               magrittr_1.5                data.table_1.12.8           DO.db_2.9                   lmtest_0.9-37              
 [78] triebeard_0.3.0             RANN_2.6.1                  fitdistrplus_1.0-14         matrixStats_0.55.0          hms_0.5.0                   lsei_1.2-0                  mime_0.7                   
 [85] GSVA_1.32.0                 xtable_1.8-4                XML_3.98-1.20               gridExtra_2.3               compiler_3.6.0              tibble_2.1.3                KernSmooth_2.23-15         
 [92] crayon_1.3.4                R.oo_1.22.0                 htmltools_0.4.0             later_1.0.0                 tidyr_1.0.0                 geneplotter_1.62.0          DBI_1.0.0                  
 [99] tweenr_1.0.1                MASS_7.3-51.1               cli_2.0.0                   R.methodsS3_1.7.1           gdata_2.18.0                metap_1.1                   igraph_1.2.4.1             
[106] GenomicRanges_1.36.1        pkgconfig_2.0.3             rvcheck_0.1.3               plotly_4.9.1                xml2_1.2.2                  foreach_1.4.7               annotate_1.62.0            
[113] XVector_0.24.0              doFuture_0.8.2              bibtex_0.4.2                stringr_1.4.0               digest_0.6.23               sctransform_0.2.0           tsne_0.1-3                 
[120] graph_1.62.0                fastmatch_1.1-0             edgeR_3.26.8                GSEABase_1.46.0             shiny_1.4.0                 gtools_3.8.1                lifecycle_0.1.0            
[127] nlme_3.1-140                outliers_0.14               jsonlite_1.6                limma_3.40.6                viridisLite_0.3.0           fansi_0.4.0                 pillar_1.4.2               
[134] lattice_0.20-38             fastmap_1.0.1               httr_1.4.1                  survival_2.43-3             GO.db_3.8.2                 glue_1.3.1                  zip_2.0.4                  
[141] UpSetR_1.4.0                png_0.1-7                   shinythemes_1.1.2           iterators_1.0.12            bit_1.1-14                  ggforce_0.2.2               stringi_1.4.3              
[148] blob_1.2.0                  singscore_1.4.0             caTools_1.17.1.2            memoise_1.1.0               irlba_2.3.3                 future.apply_1.3.0          ape_5.3     
