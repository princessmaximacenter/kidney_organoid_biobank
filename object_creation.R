setwd("tables/")
source("../functions.R")
source('../preProcFunction.R')


#########create main file with all organoids
allEarlycohortMetaMetaData <- data.frame(plateName = c( 'TM223','TM219', 'TM217', 'TM58', 'TM59'),
                                         condition= c('51E', '88E','80E', '101E', '101E'),
                                         status = c('early', 'early','early', 'early','early'),
                                         organoid = c('51',  '88','80','101','101'))



newEmpties <- c("A1","B1",'C1','D1','E1','F1','G1','H1','I1','J1','K1','L1','M1','N1','O1','P1')
oldEmpties <- c("A24","B24",'C24','D24','E24','F24','G24','H24','I24','J24','K24','L24','M24','N24','O24','P24')

empties <-   list(TM223=newEmpties,
                  TM219 = newEmpties,
                  TM217=newEmpties,
                  TM58=oldEmpties,
                  TM59=oldEmpties)


c(allEGenes, allEMeta, allEERCC, allEmito) %<-% mergeDatasets(metaMetaData = allEarlycohortMetaMetaData, min.tpx=1000, emptyWellsList = empties)


allEObj <- processSC(allEGenes, allEMeta, allEmito)

allEnfObj <- filterAllE(allEObj, dim = 8, reso = 1.35)

saveRDS(allEnfObj, file='../objects/allEnfObj.RDS')

################################## - Early late 51
EL51MetaMetaData <- data.frame(plateName = c( 'TM223', 'TM286'),
                               condition= c(  '51E', '51L'),
                               status = c(  'early','late'),
                               organoid = c('51','51'))



newEmpties <- c("A1","B1",'C1','D1','E1','F1','G1','H1','I1','J1','K1','L1','M1','N1','O1','P1')
oldEmpties <- c("A24","B24",'C24','D24','E24','F24','G24','H24','I24','J24','K24','L24','M24','N24','O24','P24')

empties <-   list(TM223=newEmpties,
                  TM286=newEmpties)


c(EL51Genes, EL51Meta, EL51ERCC, EL51mito) %<-% mergeDatasets(metaMetaData = EL51MetaMetaData, min.tpx=1000, emptyWellsList = empties)


EL51Obj <- processSC(EL51Genes, EL51Meta, EL51mito)
EL51FObj <- filterEL(EL51Obj, dim=6, reso=0.6)

saveRDS(EL51FObj, file='../objects/EL51FObj.RDS')

###################### early - late 80

EL80MetaMetaData <- data.frame(plateName = c( 'TM217', 'TM289'),
                               status= c(  '80E', '80L'),
                               passage = c( 'early', 'late'),
                               sample = c('80','80'))

newEmpties <- c("A1","B1",'C1','D1','E1','F1','G1','H1','I1','J1','K1','L1','M1','N1','O1','P1')
oldEmpties <- c("A24","B24",'C24','D24','E24','F24','G24','H24','I24','J24','K24','L24','M24','N24','O24','P24')

empties <-   list(TM217 = newEmpties,
                  TM289=newEmpties)

c(EL80genes, EL80meta, EL80ercc, EL80mito) %<-% mergeDatasets(metaMetaData = EL80MetaMetaData, min.tpx=1000, emptyWellsList = empties)

EL80Obj <- processSC(EL80genes, EL80meta, EL80mito)
EL80FObj <- filterEL(EL80Obj, dim=10, reso=0.5)

saveRDS(EL80FObj, file='../objects/EL80FObj.RDS')

######################### single organoids for alignment
allEarlycohortMetaMetaData <- data.frame(plateName = c( 'TM223','TM219', 'TM217', 'TM58', 'TM59'),
                                         condition= c('51E', '88E','80E', '101E', '101E'),
                                         status = c('early', 'early','early', 'early','early'),
                                         organoid = c('51',  '88','80','101','101'))



newEmpties <- c("A1","B1",'C1','D1','E1','F1','G1','H1','I1','J1','K1','L1','M1','N1','O1','P1')
oldEmpties <- c("A24","B24",'C24','D24','E24','F24','G24','H24','I24','J24','K24','L24','M24','N24','O24','P24')

empties <-   list(TM223=newEmpties,
                  TM219 = newEmpties,
                  TM217=newEmpties,
                  TM58=oldEmpties,
                  TM59=oldEmpties)

for (organoid in c('51', '80', '88', '101')){
  if (organoid == "ALL") {
    organoidMetaData <- allEarlycohortMetaMetaData
  } else {
    organoidMetaData <- allEarlycohortMetaMetaData[which(allEarlycohortMetaMetaData$organoid == organoid),] 
  }
  print(paste('doing organoid', organoid))
  
  c(allEGenes, allEMeta, allEERCC, allEmito) %<-% mergeDatasets(metaMetaData = organoidMetaData, min.tpx=1000, emptyWellsList = empties)
  srat <- processSC(allEGenes, allEMeta, allEmito) 
  srat <- filterAllE(srat, dim = 8, reso = 1.35)
  
  derived_Obj <- paste0("../objects/org", organoid, "_Obj.RDS")
  derived_Obj
  saveRDS(srat, file = derived_Obj)
  
}
