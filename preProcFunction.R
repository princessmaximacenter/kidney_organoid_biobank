processSC <- function(genes, metadata, mito, minTxpt=2, minCellsWithMinTxpt=2, minCellsAboveZero=5, maxPctMito=0.4,
                      GOpval=0.05, DEpval=0.05, dimensions=10, resolution=1, test.use='wilcox', noDE=TRUE, normType='logN'){
  

  #removing cells w/ high mitochondrial gene %
  cat("Removing high mitochondrial % cells...", '\n')
  c(mitoFilGenes, mitoFilMetadata, pctMito) %<-% mitoFiltering(genes, metadata, mito, maxPctMito = maxPctMito)
  cat('Done!', '\n')
  
  #removing genes with low expression
  cat("Removing lowly expresed genes...", '\n')
  mitoAbundanceFilGenes <- geneAbundanceFiltering(mitoFilGenes, minTxpt=minTxpt, 
                                                  minCellsWithMinTxpt=minCellsWithMinTxpt, 
                                                  minCellsAboveZero=minCellsAboveZero)
  cat('Done!', '\n')
  
  #creating seurat object
  cat('Creating Seurat Object...', '\n')
  sObj <- createObjectWithMetadata(mitoAbundanceFilGenes, mitoFilMetadata, pctMito)
  cat('Done!', '\n')
  
  #normalize, scale and detect vargenes
  cat("normalizing and scaling...", '\n')
  sObj <- normalizeObj(sObj, normType=normType)
  
  
  #remove cc from variable genes
  cat('Calculating and filtering variable genes...', '\n')
  sObj <- filteredVarGenes(sObj)
  cat('Done!', '\n')
  
  #remove mitochondrial pseudogenes from variable genes
  cat("Removing mitochondrial pseudogenes from variable genes...", '\n')
  sObj <- remMitoPseudogenes(sObj)
  cat("Done!", '\n')
  
  #dimensionality reduction
  cat('Performing dimensionality reduction...', '\n')
  sObj <- dimRed(sObj, 
                 dimensions=dimensions,
                 resolution=resolution)
  cat('Done!', '\n')
  
  
  #doi
  if (!noDE){
    cat('Calculating cluster markers and performing GO enrichment, this takes a bit...', '\n')
    GOProfile <- markerGO(sObj, test.use=test.use)
    cat("Done!", '\n')
    return(list(sObj, GOProfile))
  }else{
    return(sObj)
  }

  
  
}


mitoFiltering <- function(genesTable, metadataTable, mitoTable, maxPctMito=0.4){
  ##filtering cells with too much mitochondrial gene %
  #calculating total read number for mito genes
  mitoAggregate <- Matrix::colSums(mitoTable)
  names(mitoAggregate) <- colnames(mitoTable)
  
  #calculating total read number for regular features
  geneAggregate <- Matrix::colSums(genesTable)
  names(geneAggregate) <- colnames(genesTable)
  
  #calculating % mitochondrial
  pctMito <- mitoAggregate / (mitoAggregate + geneAggregate)
  
  #histogram of %
  hist(pctMito)
  
  #calculating cells to remove
  toRemoveMito <- which(pctMito >maxPctMito)
  
  #removing cells
  if (length(toRemoveMito) > 0){
    returnGenes <- genesTable[,-toRemoveMito]
    returnMetadata <- metadataTable[-toRemoveMito,]
    returnPctMito <- pctMito[-toRemoveMito]
  }
  else{
    returnGenes <- genesTable
    returnMetadata <- metadataTable
    returnPctMito <- pctMito
  }
  
  #printing results
  nRemoved <- length(toRemoveMito)
  cat('removed ', nRemoved, ' cells because they had more than ', maxPctMito*100, ' % mitochondrial reads', '\n')
  
  return(list(returnGenes, returnMetadata, returnPctMito))
}
geneAbundanceFiltering <- function(genesTable, minTxpt=2, minCellsWithMinTxpt=2, minCellsAboveZero=5){
  #filtering by number of cells expressing gene above zero
  genesTableBinaryExpression <- genesTable > 0 #calculating binary matrix 1=expressed/0=notexpressed
  gene_filter1 <- Matrix::rowSums(genesTableBinaryExpression) >= minCellsAboveZero  # how many genes are expressed in more than minCellsAboveZero cells?
  
  #filtering by number of cells expressing gene above minTxpt
  expr_data_minexpr <- genesTable >= minTxpt   # calculating binary matrix 1=more or equal to minTxpt / 0= lower than minTxpt
  gene_filter2 <- Matrix::rowSums(expr_data_minexpr) >= minCellsWithMinTxpt   # how many genes have an expression of at least minTpxt in at least minCellsWithMinTxpt cells?
  
  #combining filters
  gene_filter12 <- gene_filter1 & gene_filter2

  
  #filtering
  genesTableFiltered <- genesTable[gene_filter12, ]
  
  #reporting
  nKept <- sum(gene_filter12)
  nRemoved <- sum(!gene_filter12)
  cat(nRemoved, ' genes were removed from the dataset, ', nKept, ' were kept.', '\n')
  
  #returning 
  return(genesTableFiltered)
}
createObjectWithMetadata <- function(geneTable, metadataTable, pctMito=character(0),   projectName = ''){
  #creating the object
  sObj <- CreateSeuratObject(counts = geneTable, project = projectName) # there's a reason i don't use the meta.data argument
  
  #extracting metadata Names
  factorNames <- colnames(metadataTable)
  
  #adding metadata
  for (name in factorNames){
    sObj <- AddMetaData(sObj, metadataTable[,name], col.name=name)
  }
  
  #optionally adding pctMito
  if (length(pctMito) > 0){
    sObj <- AddMetaData(sObj, pctMito*100, col.name='pctMito')
  }
  
  return(sObj)
}

normalizeObj <- function(sObj, normType='logN'){
  
  if (normType == 'logN'){
    #normalize
    sObj <- NormalizeData(object = sObj, normalization.method = "LogNormalize",
                          scale.factor = 10000)
    #calculate variable genes 
    sObj <- FindVariableFeatures(sObj, verbose=F)
    
    #scale
    sObj <- ScaleData(sObj, verbose=F)
    
    return(sObj)
    
  }else if (normType == 'sct'){
    sObj <- SCTransform(sObj, verbose = FALSE)
    DefaultAssay(sObj) <- 'SCT'
    return(sObj)
  }
}

filteredVarGenes <- function(sObj){

  
  #get CC genes
  c(s.genes.pmc, g2m.genes.pmc) %<-% readRegevCCgenes(sObj)
  otherCCgenes <- readOtherCCgenes(sObj)
  allCCgenes <- union(union(otherCCgenes, s.genes.pmc), g2m.genes.pmc)
  
  #calculating var genes
  origVarGenes <- VariableFeatures(sObj)
  
  #filtering out cc genes
  filter_cc <- origVarGenes %in% allCCgenes
  whichGenes <- origVarGenes[origVarGenes %in% allCCgenes]
  nCCgenes <- sum(filter_cc)
  
  #setting genes
  filteredVarGenes <- origVarGenes[!(filter_cc) ]
  VariableFeatures(sObj) <- filteredVarGenes
  
  cat(nCCgenes, ' were filtered out because they are cell cycle genes', '\n')
  print(whichGenes)
  return(sObj)
}
readRegevCCgenes <- function(sObj){
  cc.genes <- readLines(con = "regev_lab_cell_cycle_genes.txt")
  s.genes <- cc.genes[1:43]
  g2m.genes <- cc.genes[44:97]
  
  #finding it in 
  searchPattern <- unlist(lapply(s.genes, function(x)paste("-",x,"$", sep='')))
  s.genes.pmc <- unlist(lapply(searchPattern, grep, rownames(sObj), value=T))
  searchPattern <- unlist(lapply(g2m.genes, function(x)paste("-",x,"$", sep='')))
  g2m.genes.pmc <-unlist(lapply(searchPattern, grep, rownames(sObj), value=T)) 
  
  return(list(s.genes.pmc, g2m.genes.pmc))
  
}
readOtherCCgenes <- function(sObj){
  cell.cycle.file <- read.table(file = "CellCycleMarkers.txt", sep="\t", stringsAsFactors = F, header=T)

  cell_cycle_genes <- unique(unlist(c(cell.cycle.file[,1:3])))
  cell_cycle_genes <- bitr(cell_cycle_genes, fromType = "SYMBOL", toType = "ENSEMBL", OrgDb = "org.Hs.eg.db")
  cell_cycle_genes <- unlist(cell_cycle_genes$ENSEMBL)
  
  #finding it in 
  searchPattern <- unlist(lapply(cell_cycle_genes, function(x)paste(x,"-", sep='')))
  ccGenes <- unlist(lapply(searchPattern, grep, rownames(sObj), value=T))
  
  return(ccGenes)
}
identGO <- function(marks, GOpval=0.05, DEpval=0.05) {
  clusters <- as.character(unique(marks$cluster))
  geneList <- list()
  for (i in clusters){
    geneList[[i]] <-  marks[marks$cluster==i & marks$p_val_adj < DEpval,'gene']
  }
  
  #geneList <- lapply(geneList, gsub, pattern='--', replacement='__')
  
  
  GOProfile <- GOCompare2(geneList, minPval = GOpval)
  
  return(GOProfile)
  
}
dimRed <- function(sObj, dimensions=10, resolution=1){
  
  
  sObj <- RunPCA(sObj, features = VariableFeatures(object = sObj), verbose=F)
  
  sObj <- FindNeighbors(sObj, dims = 1:dimensions, verbose=F)
  sObj <- FindClusters(sObj, resolution = resolution, verbose=F)
  
  
  sObj <- RunUMAP(sObj, dims=1:dimensions, verbose=F)
  sObj <- RunTSNE(sObj, dims=1:dimensions, verbose=F)
  
  print(DimPlot(sObj, group.by = 'ident'))
  print(DimPlot(sObj, group.by = 'plateName'))
  
  return(sObj)
}

markerGO <- function(sObj, test.use='wilcox'){
  
  identMarks <- FindAllMarkers(sObj, 
                               only.pos=T, 
                               logfc.threshold = 0.25, 
                               test.use=test.use, verbose=F)
  
  GOProfile <- identGO(identMarks)
  clusterProfiler::dotplot(GOProfile)
  
  return(GOProfile)
}

remMitoPseudogenes <- function(sObj, defAssay='RNA'){
  #filter out mt pseudogenes
  varGenes <- VariableFeatures(sObj)
  nRem <- length(grep("-MT(ND|CY|CO|RN)", varGenes, value=T))
  cat(nRem, ' genes were removed:', '\n')
  print(grep("-MT(ND|CY|CO|RN)", varGenes, value=T))
  varGenes <- varGenes[-grep("-MT(ND|CY|CO|RN)", varGenes)]
  
  if (length(varGenes) != 0){
    VariableFeatures(sObj) <- varGenes
  }
  
  return(sObj)
}

cellCycleScoringExt <- function(sObj){
  ##cell cycle
  cc.genes <- readLines(con = "~/regev_lab_cell_cycle_genes.txt")
  s.genes <- cc.genes[1:43]
  g2m.genes <- cc.genes[44:97]
  #
  searchPattern <- unlist(lapply(s.genes, function(x)paste("-",x,"$", sep='')))
  s.genes.pmc <- unlist(lapply(searchPattern, grep, rownames(sObj@assays$RNA@data), value=T))
  searchPattern <- unlist(lapply(g2m.genes, function(x)paste("-",x,"$", sep='')))
  g2m.genes.pmc <-unlist(lapply(searchPattern, grep, rownames(sObj@assays$RNA@data), value=T)) 
  
  #markers
  sObj <- CellCycleScoring(sObj, s.features = s.genes.pmc, g2m.features = g2m.genes.pmc,
                              set.ident = F)
  return(sObj)
}


cc_genes2 <- function(srat){
  
  # Also read in a list of cell cycle markers, from Tirosh et al, 2015
  cc.genes <- readLines(con = "~/regev_lab_cell_cycle_genes.txt")
  s.genes <- cc.genes[1:43]
  g2m.genes <- cc.genes[44:97]
  # 
  searchPattern <- unlist(lapply(s.genes, function(x)paste("-",x,"$", sep='')))
  s.genes.pmc <- unlist(lapply(searchPattern, grep, rownames(srat), value=T))
  searchPattern <- unlist(lapply(g2m.genes, function(x)paste("-",x,"$", sep='')))
  g2m.genes.pmc <-unlist(lapply(searchPattern, grep, rownames(srat), value=T)) 
  
  
  srat <- CellCycleScoring(srat, s.features = s.genes.pmc, g2m.features = g2m.genes.pmc,
                           set.ident = FALSE)
  
  #srat <- AddModuleScore(srat, list(s.genes.pmc), enrich.name = "S.score")
  #srat <- AddModuleScore(srat, list(g2m.genes.pmc), enrich.name = "G2M.score")
  
  
  # Correlation matrix
  #   --------
  cor_mat <-cor(cbind("S.score" = srat@meta.data$S.Score, 
                      "G2M.score" = srat@meta.data$G2M.Score,
                      t(as.matrix(srat@assays$RNA@data))),
                cbind("S.score" = srat@meta.data$S.Score, 
                      "G2M.score" = srat@meta.data$G2M.Score,
                      t(as.matrix(srat@assays$RNA@data))))
  
  
  boxplot(list(S_all = cor_mat[, "S.score"], 
               S_S = cor_mat[rownames(cor_mat) %in% s.genes.pmc, "S.score"],
               S_G2M = cor_mat[rownames(cor_mat) %in% g2m.genes.pmc, "S.score"],
               G2M_all = cor_mat[, "G2M.score"], 
               G2M_S = cor_mat[rownames(cor_mat) %in% s.genes.pmc, "G2M.score"],
               G2M_G2M = cor_mat[rownames(cor_mat) %in% g2m.genes.pmc, "G2M.score"]
  ))
  
  S_all = cor_mat[, "S.score"]
  S_S = cor_mat[rownames(cor_mat) %in% s.genes.pmc, "S.score"]
  S_G2M = cor_mat[rownames(cor_mat) %in% g2m.genes.pmc, "S.score"]
  G2M_all = cor_mat[, "G2M.score"]
  G2M_S = cor_mat[rownames(cor_mat) %in% s.genes.pmc, "G2M.score"]
  G2M_G2M = cor_mat[rownames(cor_mat) %in% g2m.genes.pmc, "G2M.score"]
  
  S.cutoff <- max((median(S_S) - median (S_G2M)), quantile(S_S, 0.25)); S.cutoff
  G2M.cutoff <- max((median(G2M_G2M) - median (S_G2M)),quantile(G2M_G2M, 0.25)); G2M.cutoff
  
  S.genes.derived <- rownames(cor_mat)[cor_mat[, "S.score"] >= S.cutoff &
                                         cor_mat[, "S.score"] - cor_mat[, "G2M.score"] >= 0.05]
  G2M.genes.derived <- rownames(cor_mat)[cor_mat[, "G2M.score"] >= G2M.cutoff &
                                           cor_mat[, "G2M.score"] - cor_mat[, "S.score"] >= 0.05]
  
  plot(cor_mat[, "S.score"] ~ cor_mat[, "G2M.score"], pch = 19, cex = 0.8)
  abline(h = seq(-0.2, 1, by = 0.2), lty = 2)
  abline(h = S.cutoff, lty = 2, col = "blue")
  abline(v = seq(-0.2, 1, by = 0.2), lty = 2)
  abline(v = G2M.cutoff, lty = 2, col = "red")
  points(cor_mat[rownames(cor_mat) %in% s.genes.pmc, "S.score"] ~ 
           cor_mat[rownames(cor_mat) %in% s.genes.pmc, "G2M.score"], 
         col = "blue", pch = 19)
  points(cor_mat[rownames(cor_mat) %in% g2m.genes.pmc, "S.score"] ~ 
           cor_mat[rownames(cor_mat) %in% g2m.genes.pmc, "G2M.score"], 
         col = "red", pch = 19)
  points(cor_mat[rownames(cor_mat) %in% S.genes.derived, "S.score"] ~ 
           cor_mat[rownames(cor_mat) %in% S.genes.derived, "G2M.score"], 
         col = "lightblue", pch = 19)
  points(cor_mat[rownames(cor_mat) %in% G2M.genes.derived, "S.score"] ~ 
           cor_mat[rownames(cor_mat) %in% G2M.genes.derived, "G2M.score"], 
         col = "darkred", pch = 19)
  
  cell_cycle_genes_2 <- c(s.genes.pmc, g2m.genes.pmc, S.genes.derived, G2M.genes.derived)
  return(cell_cycle_genes_2)
}

filterAllE <- function(testObj, dim=8, reso=1.35){

  cell_cycle_genes_2 <- cc_genes2(testObj)
  #var features filtering for additional cc + ribo stuff
  varGenes <- VariableFeatures(testObj)
  ccIndex <- varGenes %in% cell_cycle_genes_2
  VariableFeatures(testObj) <- varGenes[!ccIndex]
  
  varGenes <- VariableFeatures(testObj)
  RPGenes <- grep('-RP', varGenes)
  if (length(RPGenes) > 0) {
    VariableFeatures(testObj) <- varGenes[-RPGenes]
  }else{
    print("NO RP GENES REMOVED")
  }
  
  testObj <- RunPCA(testObj, features = VariableFeatures(object = testObj), verbose=F)
  testObj <- FindNeighbors(testObj, dims = 1:dim, verbose=F)
  testObj <- RunTSNE(testObj, dims=1:dim, seed.use=1)
  testObj <- FindClusters(testObj, verbose = FALSE, res=reso)
  
  return(testObj)
  
}

filterEL <- function(srat, dim=6, reso=0.6){
  

  #var features filtering for additional cc + ribo stuff
  cell_cycle_genes_2 <- cc_genes2(srat)
  varGenes <- VariableFeatures(srat)
  ccIndex <- varGenes %in% cell_cycle_genes_2
  VariableFeatures(srat) <- varGenes[!ccIndex]
  length(VariableFeatures(srat))
  
  #ribostressfiltering
  GO_df_all <- NULL
  GOs <- c("^response to unfolded protein$", "cytosolic ribosome")
  for (i in 1:length(GOs)) {
    GOTags <- reToTags(GOs[i]); GOTags
    GO_df <- AnnotationDbi::select(org.Hs.eg.db, keys = GOTags,
                                   columns = c("ENSEMBL", "ENTREZID", "SYMBOL", "GENENAME"), keytype = "GOALL")
    GO_df_all <- rbind(GO_df_all, GO_df)
  }
  
  dim(GO_df_all)
  riboPatterns <- unique(GO_df_all$ENSEMBL)
  
  varGenes <- VariableFeatures(srat)
  riboIdx <- unlist(lapply(riboPatterns,grep, varGenes))   
  if (length(riboIdx) > 0){
    VariableFeatures(srat) <- VariableFeatures(srat)[-riboIdx]
  }
  

  
  #rem sex genes
  gencode_anno <- readRDS("../../code_repo/GENCODE26_anno_PMC.RDS")
  genes_X <- c("ENSG00000229807--XIST", "ENSG00000270641--TSIX")
  ENS_Y <- gencode_anno$gene_id[gencode_anno$seqnames == "chrY"]
  genes_Y <- as.vector(apply(gencode_anno[gencode_anno$gene_id %in% ENS_Y, 
                                          c("gene_id", "gene_name")], 1, paste, collapse = "--"))
  
  remSexGenes <- union(genes_X, genes_Y)
  varGenes <- VariableFeatures(srat)
  sexIdx <- unlist(lapply(remSexGenes,grep, varGenes))   
  if (length(sexIdx) > 0){
    VariableFeatures(srat) <- VariableFeatures(srat)[-sexIdx]
  }
  
  
  srat <- RunPCA(srat, features = VariableFeatures(object = srat), verbose=T)
  srat <- FindNeighbors(srat, dims = 1:dim, verbose=F)
  srat <- RunTSNE(srat, dims=1:dim)
  srat <- FindClusters(srat, verbose = FALSE, res=reso)
  
  return(srat)
}


reToTags = function (retok, ...) {
  require(GO.db)
  allt = dbGetQuery(GO_dbconn(), "select  go_id, term from go_term")
  inds = grep(retok, as.character(allt[,"term"]), value=FALSE, ...)
  # will error if value set at call
  if (length(inds)>0) return(as.character(allt[inds,"go_id"]))
  stop("retok did not grep to any term")
}