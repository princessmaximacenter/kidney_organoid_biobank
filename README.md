# Code and figure generation
This repository consists of several files that are meant to replicate the results of the paper:

 - `functions.R` and `preProcFunction.R` are meant to be sourced and contain a lot of the functions necessary for processing.
 - `object_creation.R` will generate basic Seurat objects that are used for further processing in other files. these will be saved in the objects folder
 - `figures.R` will reproduce panels in figure 3, as well as supplementary figure 4B and 5, and Supplementary table 2.
 - `data_integration_Seurat3.R` will reproduce data integration efforts.
 -  `singleR_cell_type_barplot.R` will reproduce singleR cell type identification and bar plots (supplementary figures

in addition, the `tables` folder contains a zip file with count tables and ancillary gene sets that need to be extracted in the folder to properly work with the scripts. The `objects` folder will contain Seurat objects created by `object_creation.R` and other scripts
